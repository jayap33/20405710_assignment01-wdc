// return sorted - input result; return sorted
function returnSorted(result) {
    const sorted = _.orderBy(result, [sorted => sorted.user.toUpperCase()], ['asc']);
    return sorted;
}

// print sorted table
var sortusers = document.getElementById("sortusers");
sortusers.addEventListener("click", sortUsers);

function sortUsers() {
    sorted = returnSorted(result);
    addHdr();
    let table = document.querySelector("table");
    addRows(sorted, table);
}

const express = require('express');
const app = express();

const port = 4548;
app.use(express.static(__dirname + '/public'));

app.listen(port, () => {
    console.log(`Express server listening on port ${port} !!`);
});